/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author josevictor-ps
 */
public class OrdemPessoa extends Pessoa{
    private Pessoa[] lista;

    public OrdemPessoa(Pessoa[] lista) {
        this.lista = lista;
    }
    
    public void OrdenaIdade() {
        qsPessoa(this.lista, 0, this.lista.length-1);
    }

    private void qsPessoa(Pessoa[] vet, int ini, int fim) {
        int i, j, pivo, aux;
        i = ini;
        j = fim;
        pivo = vet[(ini+fim)/2].getIdade();
        while(i < j) {
            while(vet[i].getIdade() < pivo) i++;
            while(vet[j].getIdade() > pivo) j--;
            if(i <= j) {
                aux = vet[i].getIdade();
                vet[i].setIdade(vet[j].getIdade());
                vet[j].setIdade(aux);
                i++;
                j--;
            }
        }
        if(j > ini)qsPessoa(vet, ini, j);
        if(i < fim)qsPessoa(vet, i, fim);
    }

    public void OrdenaAlfabetica() {
        String temp;
        for (int i = 0; i < this.lista.length; i++) {
            for (int j = i + 1; j < this.lista.length; j++) 
            {
                if (this.lista[i].getNome().compareTo(this.lista[j].getNome())>0) 
                {
                    temp = this.lista[i].getNome();
                    this.lista[i].setNome(this.lista[j].getNome());
                    this.lista[j].setNome(temp);
                }
            }
        }
    }
    
    public Pessoa[] getLista() {
        return this.lista;
    }
    
}
