package classes;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author josevictor-ps
 */
public class DNA {
    private String genoma1;
    private String genoma2;

    public DNA(String genoma1, String genoma2) {
        this.genoma1 = genoma1;
        this.genoma2 = genoma2;
    }
    
    public void crossOver() {
        Random gerador = new Random();
        int marca = gerador.nextInt(genoma1.length()-2)+1;// A partir deste index
        // os genomas são trocados
        
        String aux1 = genoma2.substring(0,marca);
        String aux2 = genoma1.substring(marca);
        String aux3 = genoma1.substring(0,marca);
        String aux4 = genoma2.substring(marca);
        
        this.genoma2 = aux1 + aux2;
        this.genoma1 = aux3 + aux4;
        
    }
    
    public boolean controlCrossOver() {
        if(genoma1.length()==genoma2.length()) return false;
        else return true;
    }
    
    public void mutation() {
        Random gerador = new Random();
        int marca = gerador.nextInt(genoma1.length());
        
        String aux = "";
        for(int i=0; i<this.genoma1.length(); i++) {
            if(i==marca) {
                if(this.genoma1.charAt(i)=='A') aux+= 'T';
                else if(this.genoma1.charAt(i)=='T') aux+= 'A';
                else if(this.genoma1.charAt(i)=='C') aux+= 'G';
                else if(this.genoma1.charAt(i)=='G') aux+= 'C';
            }
            else aux += this.genoma1.charAt(i);
        }
        this.genoma1 = aux;
        
        aux = "";
        for(int i=0; i<this.genoma2.length(); i++) {
            if(i==marca) {
                if(this.genoma2.charAt(i)=='A') aux+= 'T';
                else if(this.genoma2.charAt(i)=='T') aux+= 'A';
                else if(this.genoma2.charAt(i)=='C') aux+= 'G';
                else if(this.genoma2.charAt(i)=='G') aux+= 'C';
            }
            else aux += this.genoma2.charAt(i);
        }
        this.genoma2 = aux;
    }

    public String getGenoma1() {
        return genoma1;
    }

    public String getGenoma2() {
        return genoma2;
    }
    
}
