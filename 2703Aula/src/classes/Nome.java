package classes;


import java.util.Locale;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author josevictor-ps
 */
public class Nome {
    private String nome;

    public Nome(String nome) {
        this.nome = nome;
    }
    
    
    public String abraviacao() {
        String nome = this.nome;
        String abreviacao = "";
        String palavra = "";
        
        for(int i=0; i<nome.length(); i++) {
            if(nome.charAt(i)!=' ') palavra += nome.charAt(i);
            if(nome.charAt(i)==' ') {
                if(!palavra.equals("e") && !palavra.equals("da") &&
                        !palavra.equals("do") && !palavra.equals("das") &&
                        !palavra.equals("dos") && !palavra.equals("de") &&
                        !palavra.equals("di") && !palavra.equals("du")) {
                    abreviacao += palavra.charAt(0) + ".";
                }
                palavra = "";
            }
            else if(nome.length()-1==i) {
                String maiuscula = "";
                maiuscula += palavra.charAt(0);
                maiuscula = maiuscula.toUpperCase();
                
                palavra = maiuscula +  palavra.substring(1);
                        
                abreviacao = palavra + ", " + abreviacao;
            }
        }
        
        abreviacao = abreviacao.toUpperCase();
        
        return abreviacao;
    } 
}
