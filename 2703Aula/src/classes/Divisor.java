package classes;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author josevictor-ps
 */
public class Divisor {
    private ArrayList<Integer> valores = new ArrayList();

    public ArrayList pegarValores() {
        return this.valores;
    }

    public void botarValores(int entrada) {
        for(int i=1; i<=entrada; i++) {
            if(entrada%i==0) valores.add(i);
        }
    }
    
    public String texto() {
        String vetor = "<html><p>";
        if(this.valores.size()>2) {
            for(int i=0; i<this.valores.size(); i++) {
                vetor += this.valores.get(i).toString() + " ";
                //if(vetor.length()%10==0) vetor += "<br>";
            }
            vetor += "</p></html>";
        } else vetor = "O número é primo!";
        return vetor;
    }
}
