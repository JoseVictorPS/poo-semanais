package classes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author josevictor-ps
 */
public class Texto {
    
    public int tamanho(String texto) {
        return texto.length();
    }
    
    public String caixaAlta(String texto) {
        return texto.toUpperCase();
    }
    
    public int vogais(String texto) {
        int numero = 0;
        for(int i=0; i<texto.length(); i++) {
            switch(texto.charAt(i)) {
                case 'a': case 'e': case 'i':  case 'o': case 'u':
                    numero++;
                    break;
                case 'A': case 'E': case 'I':  case 'O': case 'U':
                    numero++;
                    break;
                default:
                    break;
            }
        }
        return numero;
    }
    
    public String comecaUni(String texto) {
        boolean comeca=false;
        String resp = "Não começa com UNI";
        if(texto.length()<3) return resp;
        if((texto.charAt(0)=='U'||texto.charAt(0)=='u') &&
                (texto.charAt(1)=='N'||texto.charAt(1)=='n') &&
                (texto.charAt(2)=='I'||texto.charAt(2)=='i'))
            comeca = true;
        if(comeca)resp="Começa com UNI";
        return resp;
    }
    
    public String terminaRio(String texto) {
        boolean termina=false;
        String resp = "Não termina com RIO";
        if(texto.length()<3) return resp;
        if((texto.charAt(texto.length()-3)=='R'||texto.charAt(texto.length()-3)=='r') &&
                (texto.charAt(texto.length()-2)=='I'||texto.charAt(texto.length()-2)=='i') &&
                (texto.charAt(texto.length()-1)=='O'||texto.charAt(texto.length()-1)=='o'))
            termina = true;
        if(termina)resp = "Termina com RIO";
        return resp;
    }
    
    public int numDigitos(String texto) {
        int numero = 0;
        for(int i=0; i<texto.length(); i++) {
            switch(texto.charAt(i)) {
                case '0': case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8': case '9':
                    numero++;
                    break;
                default:
                    break;
            }
        }
        return numero;
    }
    
    public String palindromo(String texto) {
        String reversa = "";
        String resp = "Não é um palindromo";
        for(int i=texto.length()-1; i>-1; i--) reversa += texto.charAt(i);
        boolean palindromo = false;
        reversa = reversa.toUpperCase();
        String aux = texto.toUpperCase();
        if(aux.equals(reversa)) palindromo = true;
        if(palindromo) resp = "É um palindromo";
        return resp;
    }
}
