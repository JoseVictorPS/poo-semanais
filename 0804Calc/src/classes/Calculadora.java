/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author josevictor-ps
 */
public class Calculadora {
    public double calcula(String sinal, double v1, double v2) {
        double res = 0.0;
        if(sinal.equals("so")) res = v1 + v2;
        else if(sinal.equals("su")) res = v1 - v2;
        else if(sinal.equals("m")) res = v1 * v2;
        else if(sinal.equals("d")) res = v1/v2;
        return res;
    }
    
    public boolean vazio(String s) {
        if(s.equals("")) return false;
        else return true;
    }
    
}
