package com.aulaoo;

public class Main {

    public static void main(String[] args) {
        Pessoa[] turma = new Pessoa[10];
        for(int i = 0; i<10; i++) turma[i] = new Pessoa();
        turma[0].setNome("Darius");
        turma[1].setNome("Garen");
        turma[2].setNome("Katarina");
        turma[3].setNome("Ahri");
        turma[4].setNome("Yassuo");
        turma[5].setNome("Riven");
        turma[6].setNome("Fiora");
        turma[7].setNome("Aatrox");
        turma[8].setNome("Urgot");
        turma[9].setNome("Syndra");
        turma[0].setIdade(30);
        turma[1].setIdade(32);
        turma[2].setIdade(28);
        turma[3].setIdade(29);
        turma[4].setIdade(40);
        turma[5].setIdade(21);
        turma[6].setIdade(25);
        turma[7].setIdade(43);
        turma[8].setIdade(37);
        turma[9].setIdade(26);
        OrdenadorIdade idade = new OrdenadorIdade(turma);
        idade.Ordena();
        System.out.println("Ordenado por idade!");
        for(int i=0; i<10; i++) System.out.println(idade.lista[i].getIdade());
        OrdenadorNome nome = new OrdenadorNome(turma);
        nome.Ordena();
        System.out.println("\n\nOrdenado alfabeticamente!");
        for(int i=0; i<10; i++) System.out.println(nome.lista[i].getNome());
    }
}
