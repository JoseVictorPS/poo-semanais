package com.aulaoo;

public abstract class Ordenador {
    protected Pessoa[] lista;

    public Ordenador(Pessoa[] lista) {
        this.lista = lista;
    }

    public Pessoa[] getLista() {
        return this.lista;
    }

    abstract boolean compara(Pessoa fulano, Pessoa ciclano);
}
