package com.aulaoo;

public class OrdenadorNome extends Ordenador {

    public OrdenadorNome(Pessoa[] lista) {
        super(lista);
    }

    public boolean compara(Pessoa fulano, Pessoa ciclano) {
        boolean ret = false;
        if(fulano.getNome().compareTo(ciclano.getNome())>0) ret = true;
        return ret;
    }

    public void Ordena() {
        String temp;
        for (int i = 0; i < this.lista.length; i++) {
            for (int j = i + 1; j < this.lista.length; j++)
            {
                if (compara(this.lista[i], this.lista[j]))
                {
                    temp = this.lista[i].getNome();
                    this.lista[i].setNome(this.lista[j].getNome());
                    this.lista[j].setNome(temp);
                }
            }
        }
    }

}
