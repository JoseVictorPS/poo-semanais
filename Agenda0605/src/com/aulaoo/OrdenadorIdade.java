package com.aulaoo;

public class OrdenadorIdade extends Ordenador {

    public OrdenadorIdade(Pessoa[] lista) {
        super(lista);
    }

    public boolean compara(Pessoa fulano, Pessoa ciclano) {
        boolean ret = false;
        if(fulano.getIdade() > ciclano.getIdade()) ret = true;
        return ret;
    }

    public void Ordena() {
        qsPessoa(this.lista, 0, this.lista.length-1);
    }

    private void qsPessoa(Pessoa[] vet, int ini, int fim) {
        int i, j, aux;
        i = ini;
        j = fim;
        Pessoa pivo = vet[(ini+fim)/2];
        while(i < j) {
            while(compara(pivo, vet[i])) i++;
            while(compara(vet[j], pivo)) j--;
            if(i <= j) {
                aux = vet[i].getIdade();
                vet[i].setIdade(vet[j].getIdade());
                vet[j].setIdade(aux);
                i++;
                j--;
            }
        }
        if(j > ini)qsPessoa(vet, ini, j);
        if(i < fim)qsPessoa(vet, i, fim);
    }

}
